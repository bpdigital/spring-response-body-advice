package digital.breakpoint.responsebodyadvice;

public class ArticleDto {
    private Long id;
    private String title;

    public Long getId() {
        return id;
    }

    public ArticleDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ArticleDto setTitle(String title) {
        this.title = title;
        return this;
    }
}
