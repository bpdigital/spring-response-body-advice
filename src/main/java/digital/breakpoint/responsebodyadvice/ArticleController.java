package digital.breakpoint.responsebodyadvice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping
    @Dto(ArticleDto.class)
    public List<Article> getAllArticles() {
        return articleRepository.findAll();
    }

    @GetMapping("/{articleId}")
    @Dto(ArticleDto.class)
    public Article getArticle(@PathVariable Long articleId) {
        return articleRepository.getOne(articleId);
    }

}